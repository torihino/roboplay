/*
 * RoboPlay for MSX
 * Created by RoboSoft Inc. in 2025
 *
 * player.h
 */

#pragma once

void find_player_name(void);
void init_player(void);
