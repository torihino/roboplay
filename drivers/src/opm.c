/*
 * RoboPlay for MSX
 * Created by RoboSoft Inc. in 2025
 *
 * opm.c
 */

#include "support/inc/asm.h"

#include "opm.h"

#define RDSLT  0x000C
#define WRTSLT 0x0014

static Z80_registers regs;

static bool g_opm_detected = false;

bool opm_test_slot(uint8_t slot)
{
  bool result = true;
  g_opm_slot = slot;

  for(uint8_t i = 0; i < sizeof(g_opm_id_txt); i++)
  {
    regs.Bytes.A   = g_opm_slot;
    regs.UWords.HL = OPM_ID_ADDRESS + i;
    AsmCall(RDSLT, &regs, REGS_MAIN, REGS_AF);

    if(regs.Bytes.A != g_opm_id_txt[i])
    {
      result = false;
      break;
    }
  }

  return result;
}

bool opm_test_sub(uint8_t slot)
{
  bool result = false;

  for(uint8_t i = 0; i < 4 && !result; i++)
  {
    uint8_t sub_slot = 0x80 + (i << 2) + slot;
    result = opm_test_slot(sub_slot);
  }

  return result;
}

bool opm_find_slot(void)
{
  g_opm_detected = false;

  uint8_t* exptbl = (uint8_t *)0xfcc1;
  for(uint8_t i = 0; i < 4 && !g_opm_detected; i++)
  {
    if(!(exptbl[i] & 0x80))
      g_opm_detected = opm_test_slot(i);
    else
      g_opm_detected = opm_test_sub(i);
  }

  return g_opm_detected;
}

uint8_t opm_get_slot(void)
{
  return g_opm_slot;
}

void opm_reset(void)
{
  if(!g_opm_detected) return;
  
  opm_write_register(0x01, 0x20);
  opm_write_register(0x1B, 0x00);
  opm_write_register(0x18, 0x00);

  for(uint8_t i = 0; i < 8; i++)
  {
    opm_write_register(0x20 + i, 0x0);
    opm_write_register(0x08, i);
  }
}

void opm_write_register(const uint8_t reg, const uint8_t value)
{
  if(!g_opm_detected) return;

  regs.Bytes.A   = g_opm_slot;
  regs.UWords.HL = OPM_ADDRESS_REGISTER;
  regs.Bytes.E   = reg;
  AsmCall(WRTSLT, &regs, REGS_MAIN, REGS_NONE);

  while(opm_read_status_register() & 0x80);

  regs.Bytes.A   = g_opm_slot;
  regs.UWords.HL = OPM_DATA_REGISTER;
  regs.Bytes.E   = value;
  AsmCall(WRTSLT, &regs, REGS_MAIN, REGS_NONE);
}

uint8_t opm_read_status_register(void)
{
  regs.Bytes.A   = g_opm_slot;
  regs.UWords.HL = OPM_STATUS_REGISTER;
  AsmCall(RDSLT, &regs, REGS_MAIN, REGS_AF);

  return regs.Bytes.A;
}
