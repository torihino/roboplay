/*
 * RoboPlay for MSX
 * Created by RoboSoft Inc. in 2025
 *
 * mus_org.h
 *
 * MUS_ORG: FAC SoundTracker on original device(s)
 */

#pragma once

#include <stdint.h>
#include <stdbool.h>

#define OPL_WAVE_MEMORY  0x200000
#define OPL_WAVE_ADDRESS OPL_WAVE_MEMORY + 384 * 12

#define SONG_NAME_LENGTH     40
#define AUTHOR_NAME_LENGTH   40

#define NR_OF_CHANNELS 9
#define NR_OF_DRUM_CHANNELS 3

#define FIRST_DRUM_CHANNEL  6

#define BRIGHTNESS_REGISTER_INDEX 2
#define VOLUME_REGISTER_INDEX     3

#define SAMPLE_DATA_SIZE 3
#define STEP_DATA_SIZE   12
#define PATTERN_SIZE     16

#define MAX_SONG_SIZE 0x3F00

#define MAX_TEMPO 0x72

#define INSTRUMENT_DATA_SIZE 11
#define PRESET_DATA_SIZE     11

#define MUSIC_NR_OF_PRESETS        16
#define MUSIC_INSTRUMENT_DATA_SIZE 8

#define NOTE_OFF          0x40
#define DETUNE_UP         0x41
#define DETUNE_DOWN       0x42
#define PITCH_BEND_UP     0x43
#define PITCH_BEND_DOWN   0x4D
#define TEMPO_CHANGE      0x57
#define VOLUME_CHANGE     0x80
#define BRIGHTNESS_CHANGE 0xC0

#define PAN_SETTING_LEFT  0x10
#define PAN_SETTING_MID   0x30
#define PAN_SETTING_RIGHT 0x20

#define DEVICE_AUDIO  0x00
#define DEVICE_MUSIC  0x01

typedef struct
{
  uint8_t  voice_data_audio[NR_OF_CHANNELS][INSTRUMENT_DATA_SIZE];
  uint8_t  instrument_list_audio[NR_OF_CHANNELS];
  uint8_t  original_data_music[MUSIC_INSTRUMENT_DATA_SIZE];
  uint8_t  instrument_volume_music[NR_OF_CHANNELS - NR_OF_DRUM_CHANNELS];
  uint8_t  midi_drum[8];
  uint16_t speed;
  char     sample_kit_name[8];
  uint8_t  version;
  uint8_t  original_number_music;
  uint8_t  user_id;
  uint8_t  midi_register_number[NR_OF_CHANNELS];
  uint8_t  midi_id_byte[NR_OF_CHANNELS];
  uint8_t  midi_transpose_data[5];
  char     song_name[40];                     /* PRO: Length -5 */
  /* uint8_t  midi_velocity_Data[5]; */       /* PRO: First 5 channels */
  uint8_t  sustain_audio;
  char     author[40];                        /* PRO: Length -5 */
  /* uint8_t  midi_velocity_Data[5]; */       /* PRO:  Last 4 channels */
  uint8_t  device_id;
  uint8_t  number_of_tracks;
  uint8_t  midi_drum_id[7];
} MUS_HEADER;

typedef struct 
{
  uint8_t low;
  uint8_t high;
} MUS_FREQUENCY;

typedef struct
{
  bool          note_active;
  MUS_FREQUENCY note_frequency;

  uint8_t       pitch_bend_value_up;
  uint8_t       pitch_bend_value_down;
  int8_t        detune_value;
} MUS_STATUS_TABLE;

typedef struct
{
  uint8_t delta_n_high;
  uint8_t delta_n_low;
} MUS_SAMPLE_DELTA_N;

typedef struct
{
  uint8_t start_address_high;
  uint8_t start_address_low;
  uint8_t end_address_high;
  uint8_t end_address_low;
} MUS_SAMPLE_BLOCK;

typedef struct
{
  uint8_t low;
  uint8_t high;
} MUS_SAMPLE_PITCH;

typedef struct
{
  uint8_t reg;
  uint8_t data;
} MUS_PERCUSSION_DATA;

const MUS_FREQUENCY g_frequency_table[] =
{
  {0x59, 0x09}, {0x6D, 0x09}, {0x83, 0x09}, {0x9A, 0x09}, {0xB2, 0x09}, {0xCC, 0x09}, {0xE8, 0x09}, {0x05, 0x0A}, {0x23, 0x0A}, {0x44, 0x0A}, {0x67, 0x0A}, {0x8B, 0x0A}, 
  {0x59, 0x0D}, {0x6D, 0x0D}, {0x83, 0x0D}, {0x9A, 0x0D}, {0xB2, 0x0D}, {0xCC, 0x0D}, {0xE8, 0x0D}, {0x05, 0x0E}, {0x23, 0x0E}, {0x44, 0x0E}, {0x67, 0x0E}, {0x8B, 0x0E}, 
  {0x59, 0x11}, {0x6D, 0x11}, {0x83, 0x11}, {0x9A, 0x11}, {0xB2, 0x11}, {0xCC, 0x11}, {0xE8, 0x11}, {0x05, 0x12}, {0x23, 0x12}, {0x44, 0x12}, {0x67, 0x12}, {0x8B, 0x12}, 
  {0x59, 0x15}, {0x6D, 0x15}, {0x83, 0x15}, {0x9A, 0x15}, {0xB2, 0x15}, {0xCC, 0x15}, {0xE8, 0x15}, {0x05, 0x16}, {0x23, 0x16}, {0x44, 0x16}, {0x67, 0x16}, {0x8B, 0x16},
  {0x59, 0x19}, {0x6D, 0x19}, {0x83, 0x19}, {0x9A, 0x19}, {0xB2, 0x19}, {0xCC, 0x19}, {0xE8, 0x19}, {0x05, 0x1A}, {0x23, 0x1A}, {0x44, 0x1A}, {0x67, 0x1A}, {0x8B, 0x1A},
  {0x59, 0x1D}, {0x6D, 0x1D}, {0x83, 0x1D}, {0x9A, 0x1D}
};

const uint8_t g_instrument_registers[NR_OF_CHANNELS][INSTRUMENT_DATA_SIZE] =
{
  {0x20, 0x23, 0x40, 0x43, 0x60, 0x63, 0x80, 0x83, 0xA0, 0xB0, 0xC0},
  {0x21, 0x24, 0x41, 0x44, 0x61, 0x64, 0x81, 0x84, 0xA1, 0xB1, 0xC1},
  {0x22, 0x25, 0x42, 0x45, 0x62, 0x65, 0x82, 0x85, 0xA2, 0xB2, 0xC2},

  {0x28, 0x2B, 0x48, 0x4B, 0x68, 0x6B, 0x88, 0x8B, 0xA3, 0xB3, 0xC3},
  {0x29, 0x2C, 0x49, 0x4C, 0x69, 0x6C, 0x89, 0x8C, 0xA4, 0xB4, 0xC4},
  {0x2A, 0x2D, 0x4A, 0x4D, 0x6A, 0x6D, 0x8A, 0x8D, 0xA5, 0xB5, 0xC5},

  {0x30, 0x33, 0x50, 0x53, 0x70, 0x73, 0x90, 0x93, 0xA6, 0xB6, 0xC6},
  {0x31, 0x34, 0x51, 0x54, 0x71, 0x74, 0x91, 0x94, 0xA7, 0xB7, 0xC7},
  {0x32, 0x35, 0x52, 0x55, 0x72, 0x75, 0x92, 0x95, 0xA8, 0xB8, 0xC8}
};

const uint8_t g_preset_registers[NR_OF_CHANNELS][PRESET_DATA_SIZE] =
{
  {0x20, 0x23, 0x40, 0x43, 0x60, 0x63, 0x80, 0x83, 0xC0, 0xE0, 0xE3},
  {0x21, 0x24, 0x41, 0x44, 0x61, 0x64, 0x81, 0x84, 0xC1, 0xE1, 0xE4},
  {0x22, 0x25, 0x42, 0x45, 0x62, 0x65, 0x82, 0x85, 0xC2, 0xE2, 0xE5},

  {0x28, 0x2B, 0x48, 0x4B, 0x68, 0x6B, 0x88, 0x8B, 0xC3, 0xE8, 0xEB},
  {0x29, 0x2C, 0x49, 0x4C, 0x69, 0x6C, 0x89, 0x8C, 0xC4, 0xE9, 0xEC},
  {0x2A, 0x2D, 0x4A, 0x4D, 0x6A, 0x6D, 0x8A, 0x8D, 0xC5, 0xEA, 0xED},

  {0x30, 0x33, 0x50, 0x53, 0x70, 0x73, 0x90, 0x93, 0xC6, 0xF0, 0xF3},
  {0x31, 0x34, 0x51, 0x54, 0x71, 0x74, 0x91, 0x94, 0xC7, 0xF1, 0xF4},
  {0x32, 0x35, 0x52, 0x55, 0x72, 0x75, 0x92, 0x95, 0xC8, 0xF2, 0xF5}
};

const uint8_t g_drum_presets[] =
{
  0x30, 0x2a, 0x21, 0x24, 0x22, 0x23, 0x4a, 0x28, 0x32, 0x31, 0x2e, 0x2b, 0x29, 0x27, 0x4f, 0x00
};

const MUS_SAMPLE_DELTA_N g_sample_delta_n[] =
{
  {0x05, 0x22}, {0x05, 0x6a}, {0x05, 0xba}, {0x06, 0x12}, {0x06, 0x73}, {0x06, 0xd3},
  {0x07, 0x3b}, {0x07, 0xab}, {0x08, 0x1b}, {0x08, 0x9c}, {0x09, 0x24}, {0x09, 0xac},
  {0x0a, 0x3d}, {0x0a, 0xd5}, {0x0b, 0x7d}, {0x0c, 0x2d}, {0x0c, 0xe6}, {0x0d, 0xa6},
  {0x0e, 0x7f}, {0x0f, 0x57}, {0x10, 0x3f}, {0x11, 0x38}, {0x12, 0x40}, {0x13, 0x51},
  {0x14, 0x7a}, {0x15, 0xaa}, {0x16, 0xfb}, {0x18, 0x5b}, {0x19, 0xc4}, {0x1b, 0x4d},
  {0x1c, 0xfe}, {0x1e, 0xb6}, {0x20, 0x77}, {0x22, 0x70}, {0x24, 0x81}, {0x26, 0xaa},
  {0x28, 0xfc}, {0x2b, 0x5d}, {0x2d, 0xf6}, {0x30, 0xaf}, {0x33, 0x89}, {0x36, 0xa2},
  {0x39, 0xf4}, {0x3d, 0x66}, {0x40, 0xf7}, {0x44, 0xe1}, {0x49, 0x0b}, {0x4d, 0x4d},
  {0x51, 0xf0}, {0x56, 0xb2}, {0x5b, 0xec}, {0x61, 0x5f}, {0x67, 0x1a}, {0x6d, 0x45},
  {0x73, 0xe8}, {0x7a, 0xcb}, {0x81, 0xf0}, {0x89, 0xc4}, {0x92, 0x18}, {0x9a, 0xa4},
};

const MUS_SAMPLE_BLOCK g_sample_blocks[] = 
{
  {0x00, 0x00, 0x03, 0xff}, {0x04, 0x00, 0x07, 0xff}, {0x08, 0x00, 0x0b, 0xff}, {0x0c, 0x00, 0x0f, 0xff},
  {0x10, 0x00, 0x13, 0xff}, {0x14, 0x00, 0x17, 0xff}, {0x18, 0x00, 0x1b, 0xff}, {0x1c, 0x00, 0x1f, 0xff},
  {0x00, 0x00, 0x07, 0xff}, {0x08, 0x00, 0x0f, 0xff}, {0x10, 0x00, 0x17, 0xff}, {0x18, 0x00, 0x1f, 0xff},
  {0x00, 0x00, 0x0f, 0xff}, {0x10, 0x00, 0x1f, 0xff}, {0x00, 0x00, 0x1f, 0xff}
};

MUS_PERCUSSION_DATA g_percussion_data[10];

MUS_HEADER *g_mus_header;

char g_song_name[SONG_NAME_LENGTH + 1];
char g_author[AUTHOR_NAME_LENGTH + 1];

MUS_STATUS_TABLE g_mus_status_table[NR_OF_CHANNELS];

uint8_t g_speed;
uint8_t g_speed_count;

uint8_t *g_pattern_data;
uint8_t *g_song_end;

bool g_play_samples;

uint8_t g_mus_volume_table[NR_OF_CHANNELS];

const uint8_t g_factors[] = {0x39, 0x39, 0x39, 0x39, 0x4D, 0x66, 0x80, 0x99};

bool load_sample_kit(const char* file_name, const char* extension);
void play_sample(uint8_t frequency, uint8_t volume, uint8_t sample_number);

void frequency_up(uint8_t channel, uint8_t value);
void frequency_down(uint8_t channel, uint8_t value);
void handle_pitch_bend(void);

void note_on(uint8_t channel, uint8_t data);
void note_off(uint8_t channel);

void detune_up(uint8_t channel);
void detune_down(uint8_t channel);
void pitch_bend_up(uint8_t channel, uint8_t data);
void pitch_bend_down(uint8_t channel, uint8_t data);

void change_tempo(uint8_t data);
void change_volume(uint8_t channel, uint8_t data);
void change_brightness(uint8_t channel, uint8_t data);
